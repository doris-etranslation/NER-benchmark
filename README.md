# Info

This code has been tested on `Ubuntu 20.04.5 LTS`.[^1] Once everything is set up (see below), the python script should download and store the models. If the machine that does the processing does not have access to the internet, models can be downloaded, saved, and transferred using this [procedure](https://github.com/flairNLP/flair/issues/2626#issuecomment-1077477695).



## Reqs:

- Python 3.8.10
- NVIDIA Driver Version: 515.65.01 (`nvidia-smi`)
- CUDA Version: 11.7 (`nvcc --version`)
- CUDNN: 8.6.0 (`cat /usr/local/cuda/include/cudnn_version.h | grep CUDNN_MAJOR -A 2`)

## Set up

1. Create a virtual environment (or not)
2. Install python reqs: `pip install -r requirements.txt` 
3. Refer to lines 9-11 in [main.py] and uncomment the line you need for the language


## Remarks

- Loading the model (`tagger = SequenceTagger.load()` in `get_models()`) is slow, inference (in `do_ner()`) is fast -- ideally models should remain in GPU memory.
- This code uses [FlairNLP](https://github.com/flairNLP/flair) models, which are made available through the [MIT License](https://opensource.org/license/mit/). 

## Performance

With a V100 with 16GB of VRAM, the `test_performance.py` file should report this performance:
```
###########################
Model loading time: 20.12s
Inference time: 224.78s
Total time: 244.89s
###########################
```

[^1]: Specifically, this code has been tested on AWS AMI `Deep Learning AMI GPU PyTorch 1.13.1 (Ubuntu 20.04) 20230215`.