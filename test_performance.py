import os
from time import time
from flair.data import Sentence
from flair.models import SequenceTagger
from flair.splitter import SegtokSentenceSplitter
import sys

URL = "https://huggingface.co/datasets/Helsinki-NLP/tatoeba_mt/resolve/main/dev/tatoeba-dev.ces-eng.tsv"



if __name__ == "__main__":
    print("Downloading test file...")
    os.system(f"curl -o test.tsv -L {URL}")
    
    
    print("Generating data...")
    text = ""
    try:
        with open("test.tsv") as f:
            for line in f:
                text += line.split("\t")[3]
    except:
        sys.exit(f"Something failed with the file")

    toks = len(text.split())
    print(f"Launching NER on {toks} tokens...")
    start = time()
    tagger = SequenceTagger.load("flair/ner-english-large")
    splitter = SegtokSentenceSplitter()
    mod_ok = time()

    sentences = splitter.split(text)
    sentences_out = []
    tagger.predict(sentences)
    
    end = time()

    print("###########################")
    print(f"Model loading time: {round(mod_ok - start,2)}s")
    print(f"Inference time: {round(end - mod_ok,2)}s")
    print(f"Total time: {round(end - start,2)}s")
    print("###########################")

    os.system("rm test.tsv")