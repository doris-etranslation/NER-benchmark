from flair.data import Sentence
from flair.models import SequenceTagger
from flair.splitter import SegtokSentenceSplitter



def get_models():

    #tagger = SequenceTagger.load("flair/ner-english-large")
    tagger = SequenceTagger.load("flair/ner-german-large")
    #tagger = SequenceTagger.load("flair/ner-french")
    splitter = SegtokSentenceSplitter()
    return tagger, splitter


def do_ner(text):
    
    sentences = splitter.split(text)
    sentences_out = []
    tagger.predict(sentences)
    
    for sentence in sentences:
        entities = []
        tags = []
        scores = []
        start_positions = []
        end_positions = []
        sent_txt = sentence.text

        for i, en in enumerate(sentence.get_spans('ner')):
            entities.append([str(token.text) for token in en.tokens])
            tags.append(en.tag)
            scores.append(str(round(en.score, 2)))
            start_positions.append(int(en.start_position))
            end_positions.append(int(en.end_position))

        response = {'entities': entities, 'tags': tags, 'scores': scores, 'start_positions': start_positions,
                    'end_positions': end_positions}

        
        orig = text
        if len(response["entities"]) > 0: # we make sure there are entities
            
            # We do a reversed enumerate to be able to 
            # slice strings from the end so as to
            # not mess up with indices
            for i, _ in reversed(list(enumerate(response["entities"]))):
                    tag = response["tags"][i]
                    start = response["start_positions"][i]
                    end = response["end_positions"][i]
                    ent = sent_txt[start:end]
                    ent_tag = f"<{tag}>{ent}</{tag}>"
                    sent_txt = sent_txt[:start]+ent_tag+sent_txt[end:]
        
        sentences_out.append(sent_txt)

    return " ".join(sentences_out)


if __name__ == "__main__":

    tagger, splitter = get_models()
    sent = "Ursula Gertrud von der Leyen (8. Oktober 1958 in Ixelles/Elsene, Belgien) ist eine deutsche Politikerin (CDU). Seit dem 1. Dezember 2019 ist sie Präsidentin der Europäischen Kommission."
    #sent = "I like turtles. They are cool."
    tagged_sent = do_ner(sent)
    print(tagged_sent)