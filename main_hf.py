import os
from transformers import pipeline
from flair.splitter import SegtokSentenceSplitter
from huggingface_hub import login

HF_TOKEN = os.environ["HF_TOKEN"]
login(token=HF_TOKEN)


MAX_SEQ_LENGTH = 512

lang_mod = {
    'bg': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'cs': "ec-doris/xlm-roberta-large_finetuned_wikiann_slav",
    'da': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    #'de': , # covered by FLAIR
    'el': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    #'en': , # covered by FLAIR
    'es': "ec-doris/xlm-roberta-large_finetuned_wikiann_rom",
    'et': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'fi': "iguanodon-ai/bert-base-finnish-uncased-ner",
    #'fr': , # covered by FLAIR
    'ga': "jimregan/bert-base-irish-cased-v1-finetuned-ner",
    'hr': "ec-doris/xlm-roberta-large_finetuned_wikiann_slav",
    'hu': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'it': "ec-doris/xlm-roberta-large_finetuned_wikiann_rom",
    'lt': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'lv': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'mt': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'nl': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'pl': "ec-doris/xlm-roberta-large_finetuned_wikiann_slav",
    'pt': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'ro': "ec-doris/xlm-roberta-large_finetuned_wikiann_rom",
    'sk': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'sl': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
    'sv': "ec-doris/xlm-roberta-large_finetuned_wikiann_EU",
}




def get_model(lang):

    splitter = SegtokSentenceSplitter()
    
    try:
        pipe = pipeline("token-classification", model=lang_mod[lang])#, use_auth_token=True, hf_token=HF_TOKEN)
        return pipe, splitter

    except KeyError:
        print(f"{lang} does not seem to be covered")
    except Exception as e:
        print(e)


def do_ner(text):

    sentences = splitter.split(text)

    sentences_out = []
    for sentence in sentences:
        sentence = sentence.text
        
        # In case the sentence is still longer than MAX_SEQ_LENGTH
        # (with some heuristics as to word-to-token conversion)
        # we return orig sentence

        if len(sentence.split()) > MAX_SEQ_LENGTH/1.3:
            sentences_out.append(sentence)

        else:
            try: 
                # We still wrap this in a try/except in case heuristic is bad
                # with fallback to orig sentence
                tags = pipe(sentence, aggregation_strategy="first")

                if len(tags) > 0: # we make sure there are entities
                    # We do a reversed enumerate to be able to 
                    # slice strings from the end so as to
                    # not mess up with indices
                    for i, tag in reversed(list(enumerate(tags))):
                        if tag["score"] > 0.95:
                            ent_tag = tag["entity_group"]
                            start = tag["start"]
                            end = tag["end"]
                            ent = sentence[start:end] # tag["word"] --> tag["word"] when used with aggregation_strategy="first" tends to delete whitespaces in MWE
                            ent_tag = f"<{ent_tag}>{ent}</{ent_tag}>"
                            sentence = sentence[:start]+ent_tag+sentence[end:]

                sentences_out.append(sentence)
                
            except Exception as e:
                print(e)
                sentences_out.append(sentence)

    return " ".join(sentences_out)


if __name__ == "__main__":

    lang = "it"
    pipe, splitter = get_model(lang)
    sent = "Ursula von der Leyen è nata a Ixelles ed è presidente della Commissione europea. Lei è la leader del mondo libero."
    tagged_sent = do_ner(sent)
    print(lang)
    print(tagged_sent)
    print("")